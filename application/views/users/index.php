<div class="container-fluid">
  <div class="row">
    <h2 class="m-3" style="font-weight: 300; font-size: 3rem;">User Management</h2>
  </div>
  <div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
      <div class="card">
        <div class="card-header py-1">
          <h5 class="card-title py-2 mb-0 d-inline-block">List User</h5>
          <a href="<?php echo base_url('user/tambah') ?>" class="btn btn-primary float-right">Tambah User</a>
        </div>
        <div class="card-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Photo</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <!-- variabel $users didapat dari $data['users'] yg dipassing dari controller -->
              <?php foreach ($users as $user): ?>
                <tr>
                  <!-- Echo kolom dari table yg diinginkan -->
                  <td><?php echo $user->id ?></td>
                  <td><?php echo $user->name ?></td>
                  <td><?php echo $user->username ?></td>
                  <td><?php echo $user->email ?></td>
                  <td style="width: 20%"><img src="<?php echo base_url('assets/photo/users/'.$user->photo) ?>" width="100%"></td>
                  <td>
                    <a href="<?php echo base_url('user/edit/'.$user->id) ?>" class="btn btn-sm btn-success">Edit</a>
                    <a href="<?php echo base_url('users/delete/'.$user->id) ?>" class="btn btn-sm btn-danger">Hapus</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
